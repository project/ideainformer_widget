Idea Informer Widget
====================

Enables the Idea Informer feedback widget on you website. First you need to create your project at http://idea.informer.com, and then you enable this module, fill in the configuration options, and the widget is enabled.

Right now the implementation is very basic with only the most simple widget available, but that can easily be extended. I accept both co-maintainers and patches to improve the module.
